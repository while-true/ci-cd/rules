# While-True: Gitlab CI-Rules Keynotes
## General configurations
Here, you will find the steps to follow for initialize your repository with the gitlab-ci rules.

1. Create a branch named 'develop' and 'master', and protect them so maintainers could review incoming code. Set 'develop' version one minor ahead of 'master'.

2. Ensure to have runners available on your gitlab instance. At this time, we only support unix runners. If you're using global gitlab, you got a few shared runners with 2000 free minutes per month.

3. To use the core defined rules, you must include it in your own `.gitlab-ci.yml` file, and also include a technology support, lets use `npm` as example:

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/npm.yml'
```

4. Ensure to apply all requeriments defined for your desired technology, [check npm docs](/src/docs/npm.md#interface) in this case.

5. Add this "Commit message" restriction in gitlab push rules and don't modify the highlighted part.  
<code><strong><u>^<(</u></strong>task:[^\s]+|story:[^\s]+|fix:[^\s]+|release:[^\s]+|release|tag|<strong><u>hotfix:[^\s]+|hotfix|updated from:[^\s]+ to:[^\s]+( (feature|release|hotfix)+))</u></strong>( (no-pipeline|no-install|no-build|no-quality|no-pages|no-publish|no-deploy))*></code>

## Branches
You must identify two branches in where the workflow will be executed. By default those branches are 'develop' and 'master'. At least one of them must be created.  
If you create only 'master', be sure to always push hotfixes on it, because if you don't, 'release' workflow will be executed but no 'develop' branch will be updated it's minor version, and in that case 'master' recieved changes but no one version is updated or 'version' tag created, only 'latest' tag.

By the way, we recommend you to protect 'master' and 'develop', so a team leader can review incoming code and control, accept or decline all the trafic to those branches.

## Versionate table
This table shows what changes are made on branches 'develop' and 'master', and on tags 'current' and 'latest', depending on each flow.

| Flow         | develop | master  | current | latest  |
| ------------ | ------- | ------- | ------- | ------- |
| feature      | + patch |         | updated |         |
| release      | + minor |         | updated | updated |
| hotfix       |         | + patch |         | updated |
| tag          |         |         | updated | updated |

* `feature` means any change pushed to 'develop'.
* `release` means any change pushed from 'develop' to 'master'.
* `hotfix` means any change pushed to 'master' and comming from anywhere except 'develop'
* `tag` means a request to update the 'current' or 'latest' depending if target branch is 'develop' or 'master'

## Commit tag
A commit tag is an xml like tag included at the starting of a commit message. Is formed by a tag name and by space-separated attributes. Because is space-separated, is also space-sensitive. The general syntax is:  
`<{tag-name}[:tag-id][ tag-attribute]*>`

In other words, a valid commit tag must follow this regular expression:  
`^<([^\s:]+|[^\s]+:[^\s]+)+( [^\s]+)*>`

You could create your own extended expression as explained on [general configurations](#general-configurations).

## Reserved commit tag names
* `<hotfix>` or `<hotfix:###>`: Use this tag to push changes to 'master' that doesn't come from 'develop'. This tag will update 'patch' version on 'master', so if your 'develop' version is at least one 'minor' ahead from 'master', you will avoid tags and versions conflicts.
* `<tag>`: Use this tag if for any reason you don't have the tags created for your 'develop' or 'master' tags (maybe because you're initializing your repository). If pushing on 'develop', tag 'current' will be updated and a 'version' tag will be created if it doesn't exists. If pushing on 'master', tag 'latest' will be updated and a 'version' tag will be created if it doesn't exists.
* `<updated from:### to:###>`: You shouln't use this tag. It's used internally to mark a commit as a simply version update on 'develop' or 'master'. If build and publish are enabled, and target is 'develop', the 'current' version will be published. If target is 'master', the 'latest' tag will be published.

## Commit tag attributes
Here is a table with all default commit tag attributes.

| Attribute   | Description                                                 |
| ----------- | ----------------------------------------------------------- |
| no-pipeline | Disables all jobs.                                          |
| no-install  | Disables 'install' stage.                                   |
| no-build    | Disables 'build', 'quality', 'publish' and 'deploy' stages. |
| no-quality  | Disables 'quality' stage.                                   |
| no-pages    | Disables 'pages' job.                                       |
| no-publish  | Disables 'publish' stage.                                   |
| no-deploy   | Disables 'deploy' stage.                                    |

## Variables
Here is a table with all default variables.

| Variable                     | Default value                                                            | Description                                                                                                                |
| ---------------------------- | ------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------- |
| CI_ACCESS_USER               | undefined                                                                | Gitlab access username to use when performing git actions like pushing new versions or creating tags.                      |
| CI_ACCESS_TOKEN              | undefined                                                                | Gitlab access user password to use when performing git actions like pushing new versions or creating tags.                 |
| REPOSITORY_URL               | https://$CI_ACCESS_USER:$CI_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git | Url to use when performing git actions like pushing new version or creating tags.                                          |
| DISABLE_TAG                  | "false"                                                                  | Flag to disable all tags creation feature.                                                                                 |
| DISABLE_TAG_VERSION          | "false"                                                                  | Flag to disable all 'version' (like v-1.0.0) tag creation feature. This means all tags that are not 'current' or 'latest'. |
| DISABLE_TAG_VERSION_DEVELOP  | "false"                                                                  | Flag to disable all 'version' (like v-1.0.0) tag creation feature when target is 'develop'.                                |
| DISABLE_TAG_VERSION_MASTER   | "false"                                                                  | Flag to disable all 'version' (like v-1.0.0) tag creation feature when target is 'master'.                                 |
| DISABLE_TAG_CURRENT          | "false"                                                                  | Flag to disable the creation and update of 'current' tag.                                                                  |
| DISABLE_TAG_LATEST           | "false"                                                                  | Flag to disable the creation and update of 'latest' tag.                                                                   |
| DISABLE_PIPELINE             | "false"                                                                  | Flag to disable all jobs.                                                                                                  |
| DISABLE_BUILD                | "false"                                                                  | Flag to disable 'build', 'quality', 'publish' and 'deploy' stages.                                                         |
| DISABLE_INSTALL              | "false"                                                                  | Flag to disable 'install' stage.                                                                                           |
| DISABLE_QUALITY              | "false"                                                                  | Flag to disable 'quality' stage.                                                                                           |
| DISABLE_PAGES                | "false"                                                                  | Flag to disable 'pages' stage.                                                                                           |
| DISABLE_PUBLISH              | "false"                                                                  | Flag to disable 'publish' stage.                                                                                           |
| DISABLE_PUBLISH_CURRENT      | "false"                                                                  | Flag to disable 'current' job.                                                                                             |
| DISABLE_PUBLISH_LATEST       | "false"                                                                  | Flag to disable 'latest' job.                                                                                              |
| DISABLE_DEPLOY               | "false"                                                                  | Flag to disable 'deploy' stage.                                                                                            |
| VERSION_TAG_PREFIX           | "v-"                                                                     | String to use as prefix for each git version tag (like v-1.0.0).                                                           |

## Script variables
Script variables are a set of variables that will be wrapped on a function or command and evaluated at runtime. We opted to define this mechanism because yaml and gitlab-ci extension mechanisms allows you to overwrite each job 'script' property but don't allows you to extends only parts of that 'script'. Although allows you to write an 'after_script' and 'before_script', we don't necessarily want to write a script to be executed after or before but to be executed on demand of a generic script that requires only a few functionalities. In other words, by only having the possibility to overwrite a 'script' or to define an 'after' or 'before' script we loose the possibility to have a "template method" like mechanism. So beware if you're redefining any 'script' property of the jobs.  
As any variable, each of this script variables will be parsed by the runner first at job creation time and then at runtime by the core mechanism, so if you want to use any variable at runtime ensure to escape them with double '$' character.

| Variable           | Description                                                                                                                                                                                                                                             |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| CI_INSTALL         | Command triggered on install job. Should install all dependencies.                                                                                                                                                                                      |
| CI_VERSION_CURRENT | This command is triggered each time the automatic git versioning script needs to know the project's current version. Should return the project's current version                                                                                        |
| CI_VERSION_PATCH   | This command is triggered each time the automatic git versioning script needs to add one minor version to project. Should add one minor version, since the execution of this command, CI_VERSION_CURRENT command should return the new updated version. |
| CI_VERSION_MINOR   | This command is triggered each time the automatic git versioning script needs to add one patch version to project. Should add one patch version, since the execution of this command, CI_VERSION_CURRENT command should return the new updated version. |
| CI_LINT            | Command triggered on lint job. Should lint project's code.                                                                                                                                                                                              |
| CI_BUILD           | Command triggered on build job. Should build the project.                                                                                                                                                                                               |
| CI_COVER           | Command triggered on cover job. Should run tests and show the coverage.                                                                                                                                                                                 |
| CI_PAGES           | Command triggered on pages job. Should create a 'public' folder with a static documentation web page. Look at [pages job](https://gitlab.com/help/ci/yaml/README.md#pages) documentation for more information.                                          |
| CI_PUBLISH_CURRENT | Command triggered on current job. Should publish the current version on any artifacts repository.                                                                                                                                                       |
| CI_PUBLISH_LATEST  | Command triggered on latest job. Should publish the latest version on any artifacts repository.                                                                                                                                                         |
| CI_DEPLOY          | Command triggered on deploy job. Should define a mechanism that uses a variable named `DEPLOY_TO` to deploy the artifact.                                                                                                                               |

## Stages
Here is a list of each stage on first level and each corresponding job on second level.
All stages are disabled if `DISABLE_PIPELINE` variable equals "true", `no-pipeline` attribute is present in commit tag or last commit message doesn't match this pattern: `^<([^\s:]+|[^\s]+:[^\s]+)+( [^\s]+)*>`.

* **install**  
  Stage disabled when: `DISABLE_INSTALL` variable equals "true", `no-install` attribute is present in commit tag or 'version' and 'build' stages are both disabled.
  * <u>install</u>: Should install all dependencies.  
  Target branch: all.  
* **version**  
  Stage disabled when: `<updated from:### to:###>` tag is present in commit message or pipeline is triggered by gitlab web.
  * <u>feature</u>: Should update 'patch' version on 'develop'.  
  Commit tags: any but `<tag>`.  
  Target branch: 'develop'.  
  Git tags affected: 'current' and 'version'. 
  * <u>release</u>: Should update 'minor' version on 'develop'.  
  Commit tags: any but `<hotfix>`, `<hotfix:####>`, `<tag>`.  
  Target branch: 'master'.  
  Git tags affected: 'current', 'latest' and 'version'. 
  * <u>hotfix</u>: Should update 'patch' version on 'master'.   
  Commit tags: `<hotfix>` or `<hotfix:####>`.  
  Target branch: 'master'.  
  Git tags affected: 'latest' and 'version'. 
  * <u>tag</u>: Should update 'current' (if target branch is 'develop') or 'latest' (if target branch is 'master') tags, and create a 'version' tag if it doesn't exists.  
  Commit tags: `<tag>`.  
  Target branch: 'develop' or 'master'.  
  Git tags affected: 'current', 'latest' and 'version'.
* **build**  
  Stage disabled when: `DISABLE_BUILD` variable equals "true" or `no-build` attribute is present in commit tag.  
  Notice that all stage since here are disabled if this stage is disabled.
  * <u>build</u>: Should build the project. Maybe this job should create an `artifact`.  
  Commit tags: any.  
  Target branch: all.  
  * <u>lint</u>: Should lint the project.  
  Commit tags: any.  
  Target branch: all.  
* **quality**  
  Stage disabled when: `DISABLE_QUALITY` variable equals "true", `no-quality` attribute is present in commit tag or 'build' stage is disabled.  
  * <u>cover</u>: Should run unit tests and show code coverage.  
  Commit tags: any.  
  Target branch: all.  
  * <u>pages</u>: Must create a folder named 'public' with a static documentation page inside. Look at [pages job](https://gitlab.com/help/ci/yaml/README.md#pages) documentation for more information.  
  Commit tags: any.  
  Target branch: 'master'.  
  Disabled when: `DISABLE_PAGES` variable equals "true", `no-pages` attribute is present in commit or pipeline is triggered by gitlab web.  
* **publish**  
  Commit tags: `<updated from:### to:###>`.  
  Stage disabled when: `DISABLE_PUBLISH` variable equals "true", `no-publish` attribute is present in commit tag or 'build' stage is disabled or pipeline is triggered by gitlab web.  
  * <u>current</u>: Should publish an artifact once 'develop' version was correctly updated.  
  Target branch: 'develop'.  
  * <u>latest</u>: Should publish an artifact once 'master' version was correctly updated.  
  Target branch: 'master'.  
* **deploy**  
  Stage disabled when: `DISABLE_DEPLOY` variable equals "true", `no-deploy` attribute is present in commit tag or 'build' stage is disabled.  
  * <u>deploy</u>: Should deploy an artifact.  
  Target branch: all.  
  Enabled when: `DEPLOY_TO` variable is present and pipeline is triggered by gitlab web.  

## Extending
### Redefining scripts variables
Here's a basic recipe for redefining each script variable:

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'

variables:
# Ensure any of following commands to do not perform any git action.
# Ensure each one of following commands to scape '$' character with '$$' if you want to use your own script variables,
# otherwise, the runner will replace each variable before it is executed.
# ----------------------------------------------------------------
# Next command must install all dependencies.
  CI_INSTALL: |
    echo ABSTRACT
# Next command must retrieve the current version.
  CI_VERSION_CURRENT: |
    echo ABSTRACT
# Next command must update a patch version.
  CI_VERSION_PATCH: |
    echo ABSTRACT
# Next command must update a minor version.
  CI_VERSION_MINOR: |
    echo ABSTRACT
# Next command must lint the source.
  CI_LINT: |
    echo ABSTRACT
# Next command must build the source.
  CI_BUILD: |
    echo ABSTRACT
# Next command must run tests and code coverage.
  CI_COVER: |
    echo ABSTRACT
# Next command must create a "public" folder to be used as gitlab page.
  CI_PAGES: |
    echo ABSTRACT
# Next command must publish the current builded artifact.
  CI_PUBLISH_CURRENT: |
    echo ABSTRACT
# Next command must publish the latest builded artifact.
  CI_PUBLISH_LATEST: |
    echo ABSTRACT
# Next command must deploy the builded artifact.
  CI_DEPLOY: |
    echo ABSTRACT
```

### Adding jobs
To add a job to an existing stage, we offer a few base jobs for you to extend. You can extend a job by using the `extend` property:

```yaml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'

custom_job:
  extend: .base-{job-phase}
  script: do something
```

All base jobs are disabled if `DISABLE_PIPELINE` variable equals "true", `no-pipeline` attribute is present in commit tag or last commit message doesn't match this pattern: `^<([^\s:]+|[^\s]+:[^\s]+)+( [^\s]+)*>`.  
Here is a list of all base jobs.

* **.base-install**  
  Stage: install  
  Disabled when: `DISABLE_INSTALL` variable equals "true", `no-install` attribute is present in commit tag or 'version' and 'build' stages are both disabled.  
  Target branch: all.  
* **.base-version**  
  Stage: version  
  Disabled when: `<updated from:### to:###>` or `<tag>` tags are present in commit message or pipeline is triggered by gitlab web.  
  Target branch: 'develop' or 'master'.  
* **.base-build**  
  Stage: build  
  Disabled when: `DISABLE_BUILD` variable equals "true" or `no-build` attribute is present in commit tag.  
  Target branch: all.  
* **.base-quality**  
  Stage: quality  
  Disabled when: `DISABLE_QUALITY` variable equals "true", `no-quality` attribute is present in commit tag or 'build' stage is disabled.  
  Target branch: all.  
* **.base-publish-current**  
  Stage: publish-current  
  Enabled when: `<updated from:### to:### feature>` tag is present.  
  Disabled when: `DISABLE_PUBLISH` variable equals "true", `DISABLE_PUBLISH_CURRENT` variable equals "true", `no-publish` attribute is present in commit tag, 'build' stage is disabled or pipeline is triggered by gitlab web.  
  Target branch: 'develop'.  
* **.base-publish-release**  
  Stage: publish-latest  
  Enabled when: no `<updated from:### to:### release>`, `<tag>` nor `<hotfix>` tags are present.  
  Disabled when: `DISABLE_PUBLISH` variable equals "true", `DISABLE_PUBLISH_LATEST` variable equals "true", `no-publish` attribute is present in commit tag, 'build' stage is disabled or pipeline is triggered by gitlab web.  
  Target branch: 'master'.  
* **.base-publish-hotfix**  
  Stage: publish-latest  
  Enabled when: `<updated from:### to:### hotfix>` tag is present.  
  Disabled when: `DISABLE_PUBLISH` variable equals "true", `DISABLE_PUBLISH_LATEST` variable equals "true", `no-publish` attribute is present in commit tag, 'build' stage is disabled or pipeline is triggered by gitlab web.  
  Target branch: 'master'.  
* **.base-deploy**  
  Stage: deploy  
  Enabled when: `DEPLOY_TO` variable is present and pipeline is triggered by gitlab web.  
  Disabled when: `DISABLE_DEPLOY` variable equals "true", `no-deploy` attribute is present in commit tag or 'build' stage is disabled.  
  Target branch: all.  

