# While-True: Gitlab CI-Rules definitions
Here you will find documentation of the core gitlab-ci pipelines workflow.

Look at [keynotes](/src/docs/core-keynotes.md) if you already know this worflow.

## Premises
First of all you should know the necessities that this workflow was made to solve.
* **Versionate and publish**: We want our leaders to don't care about versions. All versions must be calculated, created, tagged and published automatically.
* **Integration on demand**: We don't want continuous delivery!! (In development and testing cases). Our QA team could pick a desired tag to deploy, set the target environment and then click, and deploy must succeed.
* **Quality assurance**: Each time the source changes, in any branch, the application must be builded, linted, tested and checked code coverage.
* **Extendable**: We are working with a lot of technologies such nodejs, java, python, etc. All the other premises must be covered but we need a mechanism to extend it for each one technology.
* **Configurable**: We are working in a lot of projects. Any of them must be builded, and linted, and could be deployed but some other projects not. So we need a mechanism to configure the workflow to enable and disable features as desired.

# The Gitlab CI Workflow

# Stages
## Version stage
We reduced the versionate possibilities to the following 3 paths:

1. **Feature**: When a branch named 'develop' is updated, it must update it's `patch` version, then create git tag for that new 'version' and create or update a git tag named 'current'.
2. **Release**: When a branch named 'master' is updated with changes comming from 'develop', 'develop' must update it's `minor` version, then create git tag for that new version and create or update a git tag named 'current' with 'develop' contents, and finally create or update a git tag named 'latest' with 'master' contents.
3. **Hotfix**: When a branch named 'master' is updated with changes comming from any branch except 'develop', 'master' must update it's patch version, then create a git tag for that new version and create or update a git tag named 'latest'.

This diagram shows 3 possible use cases flows:

![alt text](/src/docs/flow.svg)

## Commit tags
We defined a set of commit tag for each one of this possibilities. This tags should be added at start of each commit message on 'develop' or 'master'. This tags are like xml syntax, but limited to:  
`<{tag-name}[:tag-id][ tag-attribute]*>`

Being mandatory a `tag-name` as minimal. So, tag `<release>` is valid and `<release no-build>` too, but `< no-build>` or `<:123>` not. Also the pattern is space-sensitive, so `<release >` is not valid. In other words, the commit message must respect the following expression:  
`^<([^\s:]+|[^\s]+:[^\s]+)+( [^\s]+)*>`

That restriction allows any developer to commit any arbitriary tag, but there's a set of reserved tag names that we give a special behaviour, so we recommend you to use the following extended pattern restriction:

<code><strong><u>^<(</u></strong>task:[^\s]+|story:[^\s]+|fix:[^\s]+|release:[^\s]+|release|tag|<strong><u>hotfix:[^\s]+|hotfix|updated from:[^\s]+ to:[^\s]+( (feature|release|hotfix)+))</u></strong>( (no-pipeline|no-install|no-build|no-quality|no-pages|no-publish|no-deploy))<strong>*></strong>
</code>

Being the bolded text the part you mustn't change. The rest of this expression could be modified if you want to change or remove some features.

### Feature tags
A feature tag is each tag that doesn't match `<tag>` nor `<updated from:[^\s]+ to:[^\s]+( (feature|release|hotfix)+)>` and the target branch is 'develop'. In the case of the extended expression that we recommended previously, feature tags are:  
`<task:[^\s]+( [^\s]+)*>`  
`<story:[^\s]+( [^\s]+)*>`  
`<fix:[^\s]+( [^\s]+)*>`  
`<release:[^\s]+( [^\s]+)*>`  
`<release( [^\s]+)*>`  
`<hotfix:[^\s]+( [^\s]+)*>` *  
`<hotfix( [^\s]+)*>` *  

> \* Notice that `hotfix` tags are resolved as feature tags only if target is 'develop'.

So `<release>`, `<task:123>` and `<story:123 no-build>` are valid feature tags. Each time the last commit has a valid feature tag and target branch is 'develop', that commit is resolved by the feature path, so feature pipelines are executed, and 'develop' will update it's `patch` version.

There're three types of tags: [**release**](#release-tags), [**hotfix**](#hotfix-tags) and [**internal**](#internal-tags).

### Release tags
All feature tags, except hotfix tags, are resolved as release tags if target branch is 'master'. In other words, each tag that doesn't match `<tag>`, `<updated from:[^\s]+ to:[^\s]+( (feature|release|hotfix)+)>` nor `<(hotfix:[^\s]+|hotfix)( [^\s]+)*>` is resolved as release tag.
So `<release>`, `<task:123>` and `<story:123 no-build>` are valid release tags. Each time a commit has a valid release tag and target branch is 'master', that commit is resolved by the release path, so release pipelines are executed, and 'develop' will update it's `minor` version.

You must use any valid release tag only when accepting a merge request from 'develop' to 'master'.

### Hotfix tags
A hotfix tag is each one that follows this expression:  
`^<(hotfix:[^\s]+|hotfix)( [^\s]+)*>`  
And the target branch is 'master'.  
So `<hotfix>`, `<hotfix:123>` and `<hotfix:123 no-build>` are valid hotfix tags. Each time a commit has a valid hotfix tag and target branch is 'master', that commit is resolved by the hotfix path, so hotfix pipelines are executed, and 'master' will update it's `patch` version.

Yoy must use any valid hotfix tag when accepting a merge request from any branch except 'develop' or performing a direct push to 'master'.

### Internal tags
Finally, you should have been noticed about tags `<tag>` and `<updated from:[^\s]+ to:[^\s]+( (feature|release|hotfix)+)>`. Lets explain those separately:  
* `<tag( [^\s]+)*>`: This tag is util if you want to create a git 'version' tag that wasn't created before and update git tags 'current' or 'latest' depending on target branch. When using this tag, no version is updated or created. This will trigger the `tag` path, so 'current' git tag will be updated if pushing on 'develop' or 'latest' git tag will be updated if pushing on 'master'. In both cases, if the 'version' git tag doesn't exists, it will be created too.  
A basic use case is when you want to initialize tags at start of the project. This diagram shows the `<tag>` path:  

![alt text](/src/docs/tag-flow.svg)

* `<(updated from:[^\s]+ to:[^\s]+)( (feature|release|hotfix)+)( [^\s]+)*>`: This tag is used internally and maybe you shouldn't use it. Each time a version is updated, this tag is used as commit message and it means that a version was correctly updated, so when pushing this tag on 'develop' or 'master', a `publish` pipeline is created after build pipeline succeed. Each git tag should have this tag indicating the version update.

---
## Build stage
## Quality stage
## Publish stage
## Deploy stage
