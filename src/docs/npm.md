# While-True: npm CI rules definitions
## General configurations
Here, you will find the steps to follow to initialize your repository with our `npm` gitlab-ci rules.

1. Just include it to your `.gitlab-ci.yml` and ensure to [follow the interface](#interface) defined below.

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/npm.yml'
```

2. Set the [required variables](#variables) defined below.

## Interface
This is the `npm` scripts interface you must follow in your project to apply to our npm CI defined rules.
You must define each script as described in the next table:

| Script | Description |
| ------ | ----------- |
| name   | Must return the package name. This name is used on publish so ensure it is the same as the published package. |
| build  | Must build the project and store it inside a folder called "dist". That folder is the artifact and will be published, so ensure it has a correct package.json file. |
| lint   | Must lint the project. |
| cover  | Must run project's tests and coverage. |
| cover:percentage | Must echo project's coverage percentage. This command is executed after `cover`. |
| docs | Must create a folder named "public" or "docs" with a static web page of an API documentation for the project |
| version:current | Must return the current project's version. This version is used on publish tasks. |
| version:patch | Must update patch version. Ensure this version to be included in the "dist" folder that will be created on build job. After the execution of this job, `version:current` must return the updated version. |
| version:minor | Must update minor version. Ensure this version to be included in the "dist" folder that will be created on build job. After the execution of this job, `version:current` must return the updated version. |
| deploy:$DEPLOY_TO | Must deploy the aplication depending on `$DEPLOY_TO` runner variable. |

## Variables
Here is a set of variables added for `npm` implementation.

| Variable | Default value | Description |
| -------- | ------------- | ----------- |
| NPM_USER | undefined (required) | The `npm` user to use when publishing. |
| NPM_PASS | undefined (required) | The `npm` user's password to use when publishing. |
| NPM_EMAIL | undefined (required) | The `npm` email to use when publishing. |
| NPM_REGISTRY | https://registry.npmjs.org | The `npm` registry on where perform publish. |
