# Commands wrappers

# Must define a command called "ci-install" that executes corresponding install job script.
.ci-install: &ci-install |
  ci-install () { eval $CI_INSTALL; }

# Must define a command called "ci-lint" that executes corresponding lint job script.
.ci-lint: &ci-lint |
  ci-lint () { eval $CI_LINT; }

# Must define a command called "ci-version-current" that must return the project's current version.
.ci-version-current: &ci-version-current |
  ci-version-current () { eval $CI_VERSION_CURRENT; }

# Must define a command called "ci-version-patch" that must update the project's patch version.
# Since the execution of this command, "ci-version-current" function should return the updated version.
.ci-version-patch: &ci-version-patch |
  ci-version-patch () { eval $CI_VERSION_PATCH; }

# Must define a command called "ci-version-minor" that must update the project's minor version.
# Since the execution of this command, "ci-version-current" function should return the updated version.
.ci-version-minor: &ci-version-minor |
  ci-version-minor () { eval $CI_VERSION_MINOR; }

# Must define a command called "ci-build" that executes corresponding build job script.
.ci-build: &ci-build |
  ci-build () { eval $CI_BUILD; }

# Must define a command called "ci-cover" that executes corresponding cover job script.
.ci-cover: &ci-cover |
  ci-cover () { eval $CI_COVER; }

# Must define a command called "ci-pages" that creates a folder named "public" with a static
# content for a documentation web page.
.ci-pages: &ci-pages |
  ci-pages () { eval $CI_PAGES; }

# Must define a command called "ci-publish-current" that executes corresponding publish current job script.
.ci-publish-current: &ci-publish-current |
  ci-publish-current () { eval $CI_PUBLISH_CURRENT; }

# Must define a command called "ci-publish-latest" that executes corresponding publish latest job script.
.ci-publish-latest: &ci-publish-latest |
  ci-publish-latest () { eval $CI_PUBLISH_LATEST; }

# Must define a command called "ci-deploy" that executes corresponding deploy job script.
.ci-deploy: &ci-deploy |
  ci-deploy () { eval $CI_DEPLOY; }

# Constants
# Except expressions

# Expression to use in "except:variables" field for all jobs.
# Should be thruty if all pipeline has been deactivated.
.no-pipepline: &no-pipeline |
  $CI_COMMIT_MESSAGE =~ /^<.*no-pipeline.*>/ || $DISABLE_PIPELINE && $DISABLE_PIPELINE == "true"

# Expression to use in "except:variables" field for build jobs.
# Build jobs are all defined for stages: build, quality, publish and deploy.
# Should be thruty if build pipeline has been deactivated.
.no-build: &no-build |
  $CI_COMMIT_MESSAGE =~ /^<.*no-build.*>/ || $DISABLE_BUILD && $DISABLE_BUILD == "true"

# Expression to use in "except:variables" field for quality stage jobs.
# Should be thruty if quality pipeline has been deactivated.
.no-quality: &no-quality |
  $CI_COMMIT_MESSAGE =~ /^<.*no-quality.*>/ || $DISABLE_QUALITY && $DISABLE_QUALITY == "true"

# Expression to use in "except:variables" field for pages job.
# Should be thruty if pages job has been deactivated.
.no-pages: &no-pages |
  $CI_COMMIT_MESSAGE =~ /^<.*no-pages.*>/ || $DISABLE_PAGES && $DISABLE_PAGES == "true"

# Expression to use in "except:variables" field for install job.
# Should be thruty if install job has been deactivated.
.no-install: &no-install |
  $CI_COMMIT_MESSAGE =~ /^<.*no-install.*>/ || $DISABLE_INSTALL && $DISABLE_INSTALL == "true"

# Expression to use in "except:variables" field for publish jobs.
# Should be thruty if publish jobs has been deactivated.
.no-publish: &no-publish |
  $CI_COMMIT_MESSAGE =~ /^<.*no-publish.*>/ || $DISABLE_PUBLISH && $DISABLE_PUBLISH == "true"

# Expression to use in "except:variables" field for publish current job.
# Should be thruty if publish current job has been deactivated.
.no-publish-current: &no-publish-current |
  $DISABLE_PUBLISH_CURRENT && $DISABLE_PUBLISH_CURRENT == "true"

# Expression to use in "except:variables" field for publish latest job.
# Should be thruty if publish latest job has been deactivated.
.no-publish-latest: &no-publish-latest |
  $DISABLE_PUBLISH_LATEST && $DISABLE_PUBLISH_LATEST == "true"

# Expression to use in "except:variables" field for deploy job.
# Should be thruty if deploy job has been deactivated.
.no-deploy: &no-deploy |
  $CI_COMMIT_MESSAGE =~ /^<.*no-deploy.*>/ || $DISABLE_DEPLOY && $DISABLE_DEPLOY == "true"

# Expression to use in "except:variables" field for each job.
# Should be thruty if the commit pattern is invalid.
.no-valid-tag: &no-valid-tag |
  $CI_COMMIT_MESSAGE !~ /^<([^\s:]+|[^\s]+:[^\s]+)+( [^\s]+)*>/

# Only expressions

# Expression to use in "only:variables" field for publish jobs.
# Should be thruty if branch must be published.
.is-version-updated: &is-version-updated |
  $CI_COMMIT_MESSAGE =~ /^<updated from:[^\s]+ to:[^\s]+( (release|feature|hotfix)+)+>/

# Expression to use in "only:variables" field for publish jobs.
# Should be thruty if branch must be published.
.is-version-feature: &is-version-feature |
  $CI_COMMIT_MESSAGE =~ /^<updated from:[^\s]+ to:[^\s]+( (feature)+)+>/

# Expression to use in "only:variables" field for publish jobs.
# Should be thruty if branch must be published.
.is-version-release: &is-version-release |
  $CI_COMMIT_MESSAGE =~ /^<updated from:[^\s]+ to:[^\s]+( (release)+)+>/

# Expression to use in "only:variables" field for publish jobs.
# Should be thruty if branch must be published.
.is-version-hotfix: &is-version-hotfix |
  $CI_COMMIT_MESSAGE =~ /^<updated from:[^\s]+ to:[^\s]+( (hotfix)+)+>/

# Expression to use in "only:variables" field for hotfix job.
# Should be thruty if hotfix job must be executed.
.must-version-hotfix: &must-version-hotfix |
  $CI_COMMIT_MESSAGE =~ /^<(hotfix:[^\s]+|hotfix)( [^\s]+)*>/

# Expression to use in "only:variables" field for tag job.
# Should be thruty if tag job must be executed.
.must-version-tag: &must-version-tag |
  $CI_COMMIT_MESSAGE =~ /^<tag( [^\s]+)*>/

# Expression to use in "only:variables" field for publish jobs.
# Should be thruty if branch must be published.
.must-deploy: &must-deploy |
  $DEPLOY_TO

# Expression to use in "only:variables" field for install job.
# Should be thruty if install job must be executed so dependecies must be installed for build jobs.
.will-build: &will-build |
  $CI_COMMIT_MESSAGE !~ /^<.*no-build.*>/ && $DISABLE_BUILD == null || $DISABLE_BUILD == "false"

# Expression to use in "only:variables" field for install job.
# Should be thruty if install job must be executed so dependecies must be installed for version jobs.
.will-version: &will-version |
  $CI_COMMIT_REF_NAME =~ /^(develop|master)$/ && $CI_COMMIT_MESSAGE !~ /^<(updated).*>/

# Scripts

# Script to be runned before each job that need to update original repo.
.init-git-script: &init-git-script |
  git config --global user.email $GITLAB_USER_EMAIL;
  git config --global user.name $GITLAB_USER_NAME;
  git remote set-url origin $REPOSITORY_URL;
  git fetch -p;
  export DISABLE_TAG="${DISABLE_TAG:-false}";
  if [ "$DISABLE_TAG" != "true" ] && [ "$DISABLE_TAG" != "false" ]; then
    DISABLE_TAG=false;
  fi;
  export DISABLE_TAG_VERSION="${DISABLE_TAG_VERSION:-$DISABLE_TAG}";
  if [ "$DISABLE_TAG_VERSION" != "true" ] && [ "$DISABLE_TAG_VERSION" != "false" ]; then
    DISABLE_TAG_VERSION=false;
  fi;
  export DISABLE_TAG_VERSION_DEVELOP="${DISABLE_TAG_VERSION_DEVELOP:-$DISABLE_TAG_VERSION}";
  if [ "$DISABLE_TAG_VERSION_DEVELOP" != "true" ] && [ "$DISABLE_TAG_VERSION_DEVELOP" != "false" ]; then
    DISABLE_TAG_VERSION_DEVELOP=false;
  fi;
  export DISABLE_TAG_VERSION_MASTER="${DISABLE_TAG_VERSION_MASTER:-$DISABLE_TAG_VERSION}";
  if [ "$DISABLE_TAG_VERSION_MASTER" != "true" ] && [ "$DISABLE_TAG_VERSION_MASTER" != "false" ]; then
    DISABLE_TAG_VERSION_MASTER=false;
  fi;
  export DISABLE_TAG_CURRENT="${DISABLE_TAG_CURRENT:-$DISABLE_TAG}";
  if [ "$DISABLE_TAG_CURRENT" != "true" ] && [ "$DISABLE_TAG_CURRENT" != "false" ]; then
    DISABLE_TAG_CURRENT=false;
  fi;
  export DISABLE_TAG_LATEST="${DISABLE_TAG_LATEST:-$DISABLE_TAG}";
  if [ "$DISABLE_TAG_LATEST" != "true" ] && [ "$DISABLE_TAG_LATEST" != "false" ]; then
    DISABLE_TAG_LATEST=false;
  fi;

# Script to be runned when updating and tagging the patch version on develop.
# Previous to this script, a `ci-version-current` and `ci-version-patch` functions must be defined
# and each one must retrieve the current version and update the patch version respectively
.patch-script: &patch-script |
  FROM_VERSION=`ci-version-current`;
  ci-version-patch;
  VERSION=`ci-version-current`;
  echo "Trying to update 'develop' version from -> $FROM_VERSION to -> $VERSION [...]";
  git add . ;
  git commit -m "<updated from:$FROM_VERSION to:$VERSION feature>";
  git push $REPOSITORY_URL HEAD:develop ;
  echo "Version patch of 'develop' was updated to -> '$VERSION' [DONE]";
  VERSION_TAG_NAME="$VERSION_TAG_PREFIX$VERSION";
  TAG_EXISTS=`git tag -l "$VERSION_TAG_NAME"`;
  MUST_PUSH_TAG=false;
  if [ "$DISABLE_TAG_VERSION_DEVELOP" == "false" ]; then
    if [ ! $TAG_EXISTS ]; then
      git tag -a "$VERSION_TAG_NAME" -f -m "<version:$VERSION>";
      MUST_PUSH_TAG=true;
      echo "Defined tag $VERSION_TAG_NAME -> $VERSION";
    else
      echo "Tag -> $VERSION_TAG_NAME was already created";
    fi;
  else
    echo "Creation of tag '$VERSION_TAG_NAME' is disabled";
  fi;
  if [ "$DISABLE_TAG_CURRENT" == "false" ]; then
    git tag -a current -f -m "<version:$VERSION>";
    MUST_PUSH_TAG=true;
    echo "Defined tag current -> $VERSION";
  else
    echo "Creation of tag 'current' is disabled";
  fi;
  if [ "$MUST_PUSH_TAG" == "true" ]; then
    git push origin --tags -f;
    echo "Tags were correctly updated [DONE]";
  fi;

# Script to be runned when updating and tagging the minor version on develop.
# Previous to this script, a `ci-version-current` and `ci-version-minor` functions must be defined
# and each one must retrieve the current version and update the minor version respectively
.minor-script: &minor-script |
  FROM_VERSION=`ci-version-current`;
  if [ "$DISABLE_TAG_LATEST" == "false" ]; then
    git tag -a latest -f -m "<version:$VERSION>";
    echo "Defined tag latest -> $FROM_VERSION";
    git push origin --tags -f;
    echo "Tags were correctly updated [DONE]";
  else
    echo "Creation of tag 'latest' is disabled";
  fi;
  DEVELOP_EXISTS=`git branch -a --list "origin/develop"`;
  if [ $DEVELOP_EXISTS ]; then
    echo "Found branch 'develop', must update it's minor version";
    ci-version-minor;
    VERSION=`ci-version-current`;
    echo "Trying to update 'develop' version from -> $FROM_VERSION to -> $VERSION [...]";
    git add . ;
    git commit -m "<updated from:$FROM_VERSION to:$VERSION release>";
    git push $REPOSITORY_URL HEAD:develop;
    echo "Version minor of 'develop' was updated to -> '$VERSION' [DONE]";
    VERSION_TAG_NAME="$VERSION_TAG_PREFIX$VERSION";
    TAG_EXISTS=`git tag -l "$VERSION_TAG_NAME"`;
    MUST_PUSH_TAG=false;
    if [ "$DISABLE_TAG_VERSION_DEVELOP" == "false" ]; then
      if [ ! $TAG_EXISTS ]; then
        git tag -a "$VERSION_TAG_NAME" -f -m "<version:$VERSION>";
        MUST_PUSH_TAG=true;
        echo "Defined tag $VERSION_TAG_NAME -> $VERSION";
      else
        echo "Tag -> $VERSION_TAG_NAME was already created";
      fi;
    else
      echo "Creation of tag '$VERSION_TAG_NAME' is disabled";
    fi;
    if [ "$DISABLE_TAG_CURRENT" == "false" ]; then
      git tag -a current -f -m "<version:$VERSION>";
      MUST_PUSH_TAG=true;
        echo "Defined tag current -> $VERSION";
    else
      echo "Creation of tag 'current' is disabled";
    fi;
    if [ "$MUST_PUSH_TAG" == "true" ]; then
      git push origin --tags -f;
    echo "Tags were correctly updated [DONE]";
    fi;
  fi;

# Script to be runned when updating and tagging the patch version on master.
# Previous to this script, a `ci-version-current` and `ci-version-patch` functions must be defined
# and each one must retrieve the current version and update the patch version respectively
.hotfix-script: &hotfix-script |
  FROM_VERSION=`ci-version-current`;
  ci-version-patch;
  VERSION=`ci-version-current`;
  echo "Trying to update 'master' version from -> $FROM_VERSION to -> $VERSION [...]";
  git add . ;
  git commit -m "<updated from:$FROM_VERSION to:$VERSION hotfix>";
  git push $REPOSITORY_URL HEAD:master;
  echo "Version patch of 'master' was updated to -> '$VERSION' [DONE]";
  VERSION_TAG_NAME="$VERSION_TAG_PREFIX$VERSION";
  TAG_EXISTS=`git tag -l "$VERSION_TAG_NAME"`;
  MUST_PUSH_TAG=false;
  if [ "$DISABLE_TAG_VERSION_MASTER" == "false" ]; then
    if [ ! $TAG_EXISTS ]; then
      git tag -a "$VERSION_TAG_NAME" -f -m "<version:$VERSION>";
      MUST_PUSH_TAG=true;
      echo "Defined tag $VERSION_TAG_NAME -> $VERSION";
    else
      echo "Tag -> $VERSION_TAG_NAME was already created";
    fi;
  else
    echo "Creation of tag '$VERSION_TAG_NAME' is disabled";
  fi;
  if [ "$DISABLE_TAG_LATEST" == "false" ]; then
    git tag -a latest -f -m "<version:$VERSION>";
    echo "Defined tag latest -> $VERSION";
    MUST_PUSH_TAG=true;
  else
    echo "Creation of tag 'latest' is disabled";
  fi;
  if [ "$MUST_PUSH_TAG" == "true" ]; then
    git push origin --tags -f;
    echo "Tags were correctly updated [DONE]";
  fi;

# Script to be runned for tagging the current version on master and develop.
# Previous to this script, a `ci-version-current` function must be defined
# and must retrieve the current version
# Tag script must create `latest` tag if pushing in master or `current` if pushing
# in develop. Also will create a version tag for that repository version.
.tag-script: &tag-script |
  VERSION=`ci-version-current`;
  VERSION_TAG_NAME="$VERSION_TAG_PREFIX$VERSION";
  TAG_EXISTS=`git tag -l "$VERSION_TAG_NAME"`;
  MUST_PUSH_TAG=false;
  if [ "$CI_COMMIT_REF_NAME" == "master" ]; then
    DISABLE_TAG_REPO="$DISABLE_TAG_LATEST";
    DISABLE_TAG_FOR_VERSION="$DISABLE_TAG_VERSION_MASTER";
    TAG_NAME=latest;
  else
    DISABLE_TAG_REPO="$DISABLE_TAG_CURRENT";
    DISABLE_TAG_FOR_VERSION="$DISABLE_TAG_VERSION_DEVELOP";
    TAG_NAME=current;
  fi;
  if [ "$DISABLE_TAG_FOR_VERSION" == "false" ]; then
    if [ ! $TAG_EXISTS ]; then
      git tag -a "$VERSION_TAG_NAME" -f -m "<version:$VERSION>";
      MUST_PUSH_TAG=true;
      echo "Defined tag $VERSION_TAG_NAME -> $VERSION";
    else
      echo "Tag -> $VERSION_TAG_NAME was already created";
    fi;
  else
    echo "Creation of tag '$VERSION_TAG_NAME' is disabled";
  fi;
  if [ "$DISABLE_TAG_REPO" == "false" ]; then
    git tag -a $TAG_NAME -f -m "<version:$VERSION>";
    echo "Defined tag $TAG_NAME -> $VERSION";
    MUST_PUSH_TAG=true;
  else
    echo "Creation of tag '$TAG_NAME' is disabled";
  fi;
  if [ "$MUST_PUSH_TAG" == "true" ]; then
    git push origin --tags -f;
    echo "Tags were correctly updated [DONE]";
  fi;

.install-script: &install-script |
  ci-install

.lint-script: &lint-script |
  ci-lint

.build-script: &build-script |
  ci-build

.cover-script: &cover-script |
  ci-cover

.pages-script: &pages-script |
  ci-pages

.publish-current-script: &publish-current-script |
  ci-publish-current

.publish-latest-script: &publish-latest-script |
  ci-publish-latest

.deploy-script: &deploy-script |
  ci-deploy

# Misc
.repository-url: &repository-url |
  https://$CI_ACCESS_USER:$CI_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git

.coverage-expression: &coverage-expression |
  /Coverage:[ ]\d+%/

# Stage names
.stage-install: &stage-install |
  install

.stage-version: &stage-version |
  version

.stage-build: &stage-build |
  build

.stage-quality: &stage-quality |
  quality

.stage-publish-release: &stage-publish-release |
  publish-release

.stage-publish-feature: &stage-publish-feature |
  publish-feature

.stage-deploy: &stage-deploy |
  deploy

# Pipeline
stages:
  - *stage-install
  - *stage-version
  - *stage-build
  - *stage-quality
  - *stage-publish-release
  - *stage-publish-feature
  - *stage-deploy

variables:
  # The repository url on wich all git tasks will be performed
  # We recommend to you to don't change this.
  REPOSITORY_URL: *repository-url
  # Flag to disable all tags creation feature.
  DISABLE_TAG: "false"
  # Flag to disable all 'version' (like v-1.0.0) tag creation feature.
  # This means all tags that are not 'current' or 'latest'.
  DISABLE_TAG_VERSION: "false"
  # Flag to disable all 'version' (like v-1.0.0) tag creation feature when target is 'develop'.
  DISABLE_TAG_VERSION_DEVELOP: "false"
  # Flag to disable all 'version' (like v-1.0.0) tag creation feature when target is 'master'.
  DISABLE_TAG_VERSION_MASTER: "false"
  # Flag to disable the creation and update of 'current' tag.
  DISABLE_TAG_CURRENT: "false"
  # Flag to disable the creation and update of 'latest' tag.
  DISABLE_TAG_LATEST: "false"
  # Flag to disable all pipeline. This disables all features.
  DISABLE_PIPELINE: "false"
  # Flag to disable all build pipeline.
  # Disables 'build' stage:
  # Jobs affected: 'build' and 'lint'
  # Disables 'quality' stage, because there's no builded artifact to cover:
  # Jobs affected: 'cover' and 'pages'
  # Disables 'publish' stage, because there's no builded artifact to publish:
  # Jobs affected: 'current' and 'latest'
  # Disables 'deploy' stage, because there's no builded artifact to deploy:
  # Jobs affected: 'deploy'
  DISABLE_BUILD: "false"
  # Flag to disable dependencies install pipeline.
  # Disables 'install' stage:
  # Jobs affected: 'install'
  DISABLE_INSTALL: "false"
  # Flag to disable quality pipeline.
  # Disables 'quality' stage:
  # Jobs affected: 'cover' and 'pages'
  DISABLE_QUALITY: "false"
  # Flag to disable publish pipeline.
  # Disables 'publish' stage:
  # Jobs affected: 'current' and 'latest'
  DISABLE_PUBLISH: "false"
  # Flag to disable publish current pipeline.
  # Jobs affected: 'current'
  DISABLE_PUBLISH_CURRENT: "false"
  # Flag to disable publish latest pipeline.
  # Jobs affected: 'latest'
  DISABLE_PUBLISH_LATEST: "false"
  # Flag to disable deploy pipeline.
  # Disables 'deploy' stage:
  # Jobs affected: 'deploy'
  DISABLE_DEPLOY: "false"
  # String to use as prefix for each git version tag (like v-1.0.0).
  # Default value: "v-"
  VERSION_TAG_PREFIX: "v-"
  # Next variables must be redefined depending on each technology.
  # Ensure any of following commands to do not perform any git action.
  # Ensure each one of following commands to scape '$' character with '$$' if you want to use your own script variables,
  # otherwise, the runner will replace each variable before it is executed.
  # ----------------------------------------------------------------
  # Next command must install all dependencies.
  CI_INSTALL: echo ABSTRACT
  # Next command must retrieve the current version.
  CI_VERSION_CURRENT: echo ABSTRACT
  # Next command must update a patch version.
  CI_VERSION_PATCH: echo ABSTRACT
  # Next command must update a minor version.
  CI_VERSION_MINOR: echo ABSTRACT
  # Next command must lint the source.
  CI_LINT: echo ABSTRACT
  # Next command must build the source.
  CI_BUILD: echo ABSTRACT
  # Next command must run tests and code coverage.
  CI_COVER: echo ABSTRACT
  # Next command must create a "public" folder to be used as gitlab page.
  CI_PAGES: echo ABSTRACT
  # Next command must publish the current builded artifact.
  CI_PUBLISH_CURRENT: echo ABSTRACT
  # Next command must publish the latest builded artifact.
  CI_PUBLISH_LATEST: echo ABSTRACT
  # Next command must deploy the builded artifact.
  CI_DEPLOY: echo ABSTRACT

# JOBS
# Starting all pipeline, no-pipeline attribute disables al jobs from here
# STAGE: Install

# Use this base job to extend a job for install stage.
# A job extending this job will be added to install stage.
# Will be executed only if build or version jobs will be executed after.
# Will be disabled if all pipeline is disabled or install is disabled.
.base-install:
  stage: *stage-install
  only:
    variables:
      - *will-build
      - *will-version
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-install

install:
  stage: *stage-install
  only:
    variables:
      - *will-build
      - *will-version
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-install
  script:
    - *ci-install
    - *install-script

# STAGE: Version

# Use this base job to extend a job for version stage.
# A job extending this job will be added to version stage, except when just tagging.
# Will be executed only in 'develop' or 'master'.
# Will be disabled if all pipeline is disabled.
.base-version:
  stage: *stage-version
  only:
    refs:
      - develop
      - master
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *must-version-tag
      - *is-version-updated

feature:
  stage: *stage-version
  only:
    refs:
      - develop
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *must-version-tag
      - *is-version-updated
  script:
    - *init-git-script
    - *ci-version-current
    - *ci-version-patch
    - *patch-script

release:
  stage: *stage-version
  variables:
    GIT_STRATEGY: clone
  only:
    refs:
      - master
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *must-version-hotfix
      - *must-version-tag
      - *is-version-updated
  script:
    - *init-git-script
    - *ci-version-current
    - *ci-version-minor
    - *minor-script

hotfix:
  stage: *stage-version
  only:
    refs:
      - master
    variables:
      - *must-version-hotfix
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *must-version-tag
      - *is-version-updated
  script:
    - *init-git-script
    - *ci-version-current
    - *ci-version-patch
    - *hotfix-script

tag:
  stage: *stage-version
  only:
    refs:
      - master
      - develop
    variables:
      - *must-version-tag
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *is-version-updated
  script:
    - *init-git-script
    - *ci-version-current
    - *tag-script

# Starting build stages, no-build attribute disables all jobs from here
# STAGE: Build

# Use this base job to extend a job for build stage.
# A job extending this job will be added to build stage.
# Will be executed on all branches.
# Will be disabled if all pipeline is disabled.
# Will be disabled if build stage is disabled.
.base-build:
  stage: *stage-build
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build

build:
  stage: *stage-build
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
  script:
    - *ci-build
    - *build-script

lint:
  stage: *stage-build
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
  script:
    - *ci-lint
    - *lint-script

# STAGE: Quality - no-quality attribute to disable

# Use this base job to extend a job for quality stage.
# A job extending this job will be added to quality stage.
# Will be executed on all branches.
# Will be disabled if all pipeline is disabled.
# Will be disabled if build stage is disabled.
# Will be disabled if quality stage is disabled.
.base-quality:
  stage: *stage-quality
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-quality

cover:
  stage: *stage-quality
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-quality
  script:
    - *ci-cover
    - *cover-script
  coverage: *coverage-expression

pages:
  stage: *stage-quality
  only:
    refs:
      - master
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-quality
      - *no-pages
  script:
    - *ci-pages
    - *pages-script
  artifacts:
    paths:
      - public

# STAGE: Publish current

# Use this base job to extend a job for publish stage.
# A job extending this job will be added to publish stage.
# Will be executed on 'develop'.
# Will be executed if 'updated' tag is present on commit message with 'feature' attribute.
# Will be disabled if it's triggered by gitlab web.
# Will be disabled if all pipeline is disabled.
# Will be disabled if build stage is disabled.
# Will be disabled if publish stage is disabled.
.base-publish-current:
  stage: *stage-publish-feature
  only:
    refs:
      - develop
    variables:
      - *is-version-feature
      - *is-version-release
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-publish

current:
  stage: *stage-publish-feature
  only:
    refs:
      - develop
    variables:
      - *is-version-feature
      - *is-version-release
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-publish
      - *no-publish-current
  script:
    - *ci-publish-current
    - *publish-current-script

# STAGE: Publish latest

# Use this base job to extend a job for publish stage.
# A job extending this job will be added to publish stage.
# Will be executed on 'develop'.
# Will be executed if 'updated' tag is present on commit message with 'release' attribute.
# Will be disabled if it's triggered by gitlab web.
# Will be disabled if all pipeline is disabled.
# Will be disabled if build stage is disabled.
# Will be disabled if publish stage is disabled.
.base-publish-release:
  stage: *stage-publish-release
  only:
    refs:
      - master
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-publish
      - *no-publish-latest
      - *must-version-hotfix
      - *must-version-tag
      - *is-version-updated


# Use this base job to extend a job for publish stage.
# A job extending this job will be added to publish stage.
# Will be executed on 'master'.
# Will be executed if 'updated' tag is present on commit message with 'hotfix' attribute.
# Will be disabled if it's triggered by gitlab web.
# Will be disabled if all pipeline is disabled.
# Will be disabled if build stage is disabled.
# Will be disabled if publish stage is disabled.
.base-publish-hotfix:
  stage: *stage-publish-release
  only:
    refs:
      - master
    variables:
      - *is-version-hotfix
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-publish
      - *no-publish-latest

latest-release:
  stage: *stage-publish-release
  only:
    refs:
      - master
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-publish
      - *no-publish-latest
      - *must-version-hotfix
      - *must-version-tag
      - *is-version-updated
  script:
    - *ci-publish-latest
    - *publish-latest-script

latest-hotfix:
  stage: *stage-publish-release
  only:
    refs:
      - master
    variables:
      - *is-version-hotfix
  except:
    refs:
      - web
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-publish
      - *no-publish-latest
  script:
    - *ci-publish-latest
    - *publish-latest-script

# STAGE: Deploy

# Use this base job to extend a job for deploy stage.
# A job extending this job will be added to deploy stage.
# Will be executed on 'develop' or 'master'.
# Will be disabled if all pipeline is disabled.
# Will be disabled if build stage is disabled.
# Will be disabled if deploy stage is disabled.
.base-deploy:
  stage: *stage-deploy
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-deploy

deploy:
  stage: *stage-deploy
  only:
    refs:
      - web
    variables:
      - *must-deploy
  except:
    variables:
      - *no-valid-tag
      - *no-pipeline
      - *no-build
      - *no-deploy
  script:
    - *ci-deploy
    - *deploy-script
