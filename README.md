# While-True: CI-Rules
The responsibility of this repository is to store a core gitlab-ci rules configurations, and it's respectives implementations for different technologies.

## Keynotes
Are you just looking for the [keynotes](/src/docs/core-keynotes.md)? ;)

## Quickstart
To use our core defined rules, you must include it in your own `.gitlab-ci.yml` file:

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'
```

This will include the core ci-rules definition from the `latest` stable tag, for unix runners.
If you want to use a specific tag or to use a non unix runner, you can change the gitlab url:

`'https://gitlab.com/while-true/ci-rules/raw/[v-x.y.z|latest]/src/[sh|cmd]/core.yml'`

> At this time, there are no `cmd` core yml definitions, so we are not supporting that yet.

By including the `core.yml` file, you have now all the base jobs and pipelines defined by [our ci-flow](/src/docs/core.md).

But we are not done yet, maybe you want at least a basic automatic versionate feature. So, suppossing you want to use npm to manage your versions, you can include [our npm-flow](/src/docs/npm.md) yml:

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/npm.yml'
# Also, we disable `build` and `install` features, because we only want to automatic versionate.
variables:
  DISABLE_BUILD: "true"
  DISABLE_INSTALL: "true"
```

You can change the tag and OS by the same way for `core.yml`. We recommend to you to use the same in both includes.

Now you must implement a minimal `npm` scripts interface, so add the following package.json:

```json
{
  "name": "your-package-name",
  "version": "0.0.0",
  "scripts": {
    "version:current": "node -p \"require('./package.json').version\"",
    "version:patch": "npm version patch --no-git-tag-version",
    "version:minor": "npm version minor --no-git-tag-version"
  }
}
```

Finally, you should add the following commit pattern restriction:  
<code><strong><u>^<(</u></strong>task:[^\s]+|story:[^\s]+|fix:[^\s]+|release:[^\s]+|release|tag|<strong><u>hotfix:[^\s]+|hotfix|updated from:[^\s]+ to:[^\s]+( (feature|release|hotfix)+))</u></strong>( (no-pipeline|no-install|no-build|no-quality|no-pages|no-publish|no-deploy))*></code>

> If you have only `master` branch, you should commit only hotfixes to it, so ensure to start any commit with `<hotfix>` tag. Otherwise, any commit to `master` will try to update minor version on `develop` and will not update `master` version.

> You can disable commit tags by simply removing it from the above expression, but be sure to not remove highlighted expression, because it has special meanings.

> Ensure to enable `install` feature if you add dependencies to your package.json

And voilà!! You are now implementing automatic versionate on your project via npm and the package.json file.

## Documentation and implementations

* [`core`](/src/docs/core.md): This is the core documentation of our own gitlab ci/cd flow.
* [`npm`](/src/docs/npm.md): This is the implementation of `core` to be used within an `npm` based project.
* Comming soon: More implementations of core for different technologies and OS. See [contributing file](/CONTRIBUTING.md) if you want to be part.
