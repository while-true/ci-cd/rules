# Contributing
If you want to contribute, please follow the rules defined in this document.

## Core
All implementations must follow and extends the [core](/src/docs/core.md) definitions, and each one must be included with core by the following way:

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/npm.yml'
```

Being `npm.yml` the implementation, and mustn't depend on other yml configurations.

Also you can extend and specialize existing implementation:

```yml
include:
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/core.yml'
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/npm.yml'
  - remote: 'https://gitlab.com/while-true/ci-rules/raw/latest/src/sh/angular.yml'
```
Because an Angular application maybe needs the `npm` flow and maybe should add it's custom pipelines and configurations.

## Foldering
### Adding a new OS support
If you want to add a new OS support, you must create a folder in `/src` with a precise name. So, suppossing you want to add support to windows batch, you maybe should create a folder named `/src/cmd`, or something like that.

Each OS support folder must include at least a file named `core.yml` with the corresponding implementation of our [core rules](/src/docs/core.md).

### Adding a new technology support
If you want to add a new technology support, you must create a file in the corresponding OS folder with a precise name. So, suppossing you want to add support to `maven`, you must add a file named `/src/sh/mvn.yml`, or something like that.

Each technology implementation must have it's own documentation `md` file, placed in `/src/docs` with the same name. If a `md` file already exists for your desired implementation name, it must respect that documentation, so you cannot use the same name if you want other implementation for the same technology.

Here's a basic recipe for creating a new technology support:
```yml

variables:
# Ensure any of following commands to do not perform any git action.
# Ensure each one of following commands to scape '$' character with '$$' if you want to use your own script variables,
# otherwise, the runner will replace each variable before it is executed.
# ----------------------------------------------------------------
# Next command must install all dependencies.
  CI_INSTALL: |
    echo ABSTRACT
# Next command must retrieve the current version.
  CI_VERSION_CURRENT: |
    echo ABSTRACT
# Next command must update a patch version.
  CI_VERSION_PATCH: |
    echo ABSTRACT
# Next command must update a minor version.
  CI_VERSION_MINOR: |
    echo ABSTRACT
# Next command must lint the source.
  CI_LINT: |
    echo ABSTRACT
# Next command must build the source.
  CI_BUILD: |
    echo ABSTRACT
# Next command must run tests and code coverage.
  CI_COVER: |
    echo ABSTRACT
# Next command must create a "public" folder to be used as gitlab page.
  CI_PAGES: |
    echo ABSTRACT
# Next command must publish the current builded artifact.
  CI_PUBLISH_CURRENT: |
    echo ABSTRACT
# Next command must publish the latest builded artifact.
  CI_PUBLISH_LATEST: |
    echo ABSTRACT
# Next command must deploy the builded artifact.
  CI_DEPLOY: |
    echo ABSTRACT
```
